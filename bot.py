#!/usr/bin/env python3
"""Social net bot"""
import configparser
import json
import logging
import random
import requests
import string

from collections import OrderedDict


config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read("bot.ini")

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class SocialNetBot:

    def __init__(self, conf):
        self.config = conf
        self.api_root = self.config['Api']['api_root']
        self.number_of_users = int(self.config['DEFAULT']['number_of_users'])
        self.max_posts_per_user = int(self.config['DEFAULT']['max_posts_per_user'])
        self.max_likes_per_user = int(self.config['DEFAULT']['max_likes_per_user'])
        self.USERS = OrderedDict()
        self.POSTS = OrderedDict()
        self.HEADERS = {'Content-Type': 'application/json', 'Accept': 'application/json'}

    @staticmethod
    def make_json(data):
        json_data = json.loads(json.dumps(data, ))
        return json_data

    def signup_user(self, username, email, password):
        url = self.api_root + 'register/'
        data = self.make_json({'username': username, 'email': email, 'password': password, 'password2': password})
        response = requests.post(url, json=data, headers=self.HEADERS)
        response_body = response.json()
        if response.status_code == 201:
            self.USERS[username] = {'email': email, 'password': password}
            msg = f'{username} signed up'
            logger.info(msg)
        else:
            msg = f'signup_user {username}: {response_body}'
            logger.error(msg)

    def signin_user(self, username, password):
        url = self.api_root + 'login/'
        data = self.make_json({'username': username, 'password': password})
        response = requests.post(url, json=data, headers=self.HEADERS)
        response_body = response.json()
        if response.status_code == 200:
            self.USERS[username]['token'] = response_body.get('access')
            msg = f'{username} signed in'
            logger.info(msg)
        else:
            msg = f'signin_user {username}: {response_body}'
            logger.error(msg)

    def create_post(self, user, title, content):
        url = self.api_root + 'posts/'
        token = self.USERS[user]['token']
        headers = {**self.HEADERS, 'Authorization': f'Bearer {token}'}
        data = self.make_json({'title': title, 'content': content})
        response = requests.post(url, json=data, headers=headers)
        response_body = response.json()
        if response.status_code == 201:
            post_id = response_body.get('id')
            self.POSTS[post_id] = {'title': title, 'content': content, 'author': user}
            msg = f'{user} created post {title}'
            logger.info(msg)
        else:
            msg = f'create_post by {user} post {title}: {response_body}'
            logger.error(msg)

    def like_post(self, post_id, user):
        url = f'{self.api_root}posts/{post_id}/like_dislike/'
        token = self.USERS[user]['token']
        headers = {**self.HEADERS, 'Authorization': f'Bearer {token}'}
        response = requests.post(url, headers=headers)
        response_body = response.json()
        if response.status_code == 200:
            act = response_body.get('act')
            msg = f'post {post_id} {act}d by {user}'
            logger.info(msg)
        else:
            msg = f'like_post {post_id} by {user}: {response_body}'
            logger.error(msg)

    def create_users(self):
        for u in range(1, self.number_of_users):
            username = id_generator()
            email = f'{username}@example.com'
            password = f'{username}{username}'
            self.signup_user(username, email, password)

    def login_users(self):
        for user in self.USERS.keys():
            self.signin_user(user, self.USERS[user]['password'])

    def create_posts(self):
        for user in self.USERS:
            posts_num = random.randint(0, self.max_posts_per_user)
            if posts_num:
                for p in range(1, posts_num+1):
                    title = f'title{p}_{user}'
                    content = f'content_of_{title}_{user}'
                    self.create_post(user, title, content)

    def create_likes(self):
        for user in self.USERS:
            likes_num = random.randint(0, self.max_likes_per_user)
            if likes_num and self.POSTS:
                posts_to_like = random.choices(list(self.POSTS.keys()), k=likes_num)
                for post in posts_to_like:
                    self.like_post(post, user)

    def run(self):
        self.create_users()
        self.login_users()
        self.create_posts()
        self.create_likes()


if __name__ == '__main__':
    bot = SocialNetBot(config)
    try:
        bot.run()
        logger.info("The bot job has successfully finished")
    except Exception as e:
        logger.exception("Something went wrong")

