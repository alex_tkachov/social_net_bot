Social Network Bot
=============================================

This is a social network bot.
It emulates users activity communicating with `API`_ .


.. _API: https://bitbucket.org/alex_tkachov/social_net/

Getting up and running
----------------------

NOTE: Requires `virtualenv,`_ virtualenvwrapper_.

.. _`virtualenv,`: http://virtualenv.readthedocs.org/en/latest/
.. _virtualenvwrapper: http://virtualenvwrapper.readthedocs.org/en/latest/

The steps below will get you up and running with a local development environment. It is assumed you have the following installed:

* pip
* virtualenv

First make sure to create and activate a virtualenv_::

    $ mkvirtualenv --python=/usr/bin/python3 social_net_bot

Then open a terminal at the project root and install the requirements for local running::

    $ pip install -r requirements.txt

.. _virtualenv: http://docs.python-guide.org/en/latest/dev/virtualenvs/

One can now run the application locally::

    $ python -m bot

